<?php

namespace App\Services;

use Auth;

class LoginService
{
    public function userLogin($credentials) {
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $response = [
                'success' => true,
                'message' => 'Login Success',
                'code' => 200,
                'data' =>$user,
                'token' => $user->createToken('Personal Access Token')->accessToken,
            ];
        }
        else {
            $response = [
                'success' => false,
                'message' => 'Unauthorized',
                'code' => 401,
                'data' => [],
            ];
        }

        return $response;
    }
}