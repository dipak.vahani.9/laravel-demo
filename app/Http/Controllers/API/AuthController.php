<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Services\LoginService;

class AuthController extends Controller
{
    public function login(LoginRequest $request) {
        $credentials = ['email'=>$request->email, 'password'=>$request->password];

        $loginService = new LoginService;

        $response = $loginService->userLogin($credentials);

        return response()->json($response, $response['code']);
    }
}
