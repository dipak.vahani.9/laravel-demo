# Laravel Demo Project Setup

Welcome to Laravel Demo Project! This document will guide you through the setup process for this Laravel project.

## Prerequisites

Make sure you have the following installed on your machine:

- [PHP](https://www.php.net/manual/en/install.php)
- [Composer](https://getcomposer.org/)

## Getting Started

- **Clone the repository:**

    ```bash
    git clone 
    ```

- **Navigate to the project folder:**

    ```bash
    cd laravel-demo
    ```

- **Install PHP dependencies:**

    ```bash
    composer install
    ```

- **Copy the example environment file and configure:**

    ```bash
    cp .env.example .env
    ```

    Update the `.env` file with your database credentials and other configuration settings.

- **Generate application key:**

    ```bash
    php artisan key:generate
    ```

- **Run database migrations:**

    ```bash
    php artisan migrate
    ```

- **Run database seeder:**

    ```bash
    php artisan db:seed
    ```

- **Generate passport key:**

    ```bash
    php artisan passport:install
    ```

- **Serve the application:**

    ```bash
    php artisan serve
    ```

    Visit [http://localhost:8000](http://localhost:8000) in your browser.