<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Hash;

class UsersTableSeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Dummy User',
            'email' => 'dummyuser@example.com',
            'email_verified_at' => now(),
            'password' => Hash::make('654789'),
        ]);
    }
}
